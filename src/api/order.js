import request from '@/utils/request'
export function fetchList(params) {
  return request({
    url:'/orders/list',
    method:'get',
    params:params
  })
}

export function closeOrder(params) {
  return request({
    url:'/orders/shutdown',
    method:'post',
    params:params
  })
}

export function deleteOrder(params) {
  return request({
    url:'/orders/delete',
    method:'post',
    params:params

  })
}

export function updateShopMark(params) {
  return request({
    url:'/orders/bisnote',
    method:'post',
    params:params
 
  })
}

export function deliveryOrder(params) {
  return request({
    url:'/orders/delivery',
    method:'post',
    params:params
  });
}

export function getDeliveryOrder(id){
  return request({
    url:`/orders/getInfo/${id}`,
    method:'get'
  })
}


export function snifOrder(id){
  return request(
    {
      url:'/orders/Sniffing/'+id,
      method:'post', 
    }
  )
}

export function getOrderDetail(id) {
  return request({
    url:'/order/'+id,
    method:'get'
  });
}

export function getReceiverInfo(id) {
  return request({
    url:`/orders/getAddress/${id}`,
    method:'get'
  })
}

export function getProducts(id) {
  return request({
    url:`/orders/getProducts/${id}`,
    method:'get'
  })
}

export function getSetting(){
  return request({
    url:'/orders/getSetting',
    method:'get'
  })
}
export function updateReceiverInfo(data) {
  return request({
    url:'/order/update/receiverInfo',
    method:'post',
    data:data
  });
}

export function updateMoneyInfo(data) {
  return request({
    url:'/order/update/moneyInfo',
    method:'post',
    data:data
  });
}

export function updateOrderNote(params) {
  return request({
    url:'/order/update/note',
    method:'post',
    params:params
  })
}

export function getRiders(params) {
  return request({
    url:'riderm/whoIsWorking',
    method:'get',
    params:params
  })
}