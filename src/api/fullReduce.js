import request from '@/utils/request'
export function fetchList(params) {
  return request({
    url:'/coupon/listFR',
    method:'get',
    params:params
  })
}

export function updateStatus(data) {
  return request({
    url:'/coupon/statusFR',
    method:'post',
    data:data,
    // headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
  })
}

export function createFullReduce(data) {
    return request({
      url:'/coupon/addFR',
      method:'post',
      data:data,
      // headers: {
      //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
      //  },
    })
  }
  
  export function updateFullReduce(data) {
    return request({
      url:'/coupon/updateFR',
      method:'post',
      data:data,
      // headers: {
      //     'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
      //    },
    })
  }
  
  export function deleteFullReduce(data) {
    return request({
      url:'/coupon/deleteFR',
      method:'post',
      params:data,
    //   headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
    })
  }
  