import request from '@/utils/request'
export function fetchList(params) {
  return request({
    url:'/coupon/listHistory',
    method:'get',
    params:params
  })
}

export function fetchCouponList(params) {
  return request({
    url:'/coupon/searchByOno',
    method:'get',
    params:params
  })
}
