import request from '@/utils/request'
export function policy(data) {
  return request({
    url:'/oss/upload',
    method:'post',
    data:data,
    headers: {
      'Content-Type': 'multipart/form-data'
  }
  })
}
