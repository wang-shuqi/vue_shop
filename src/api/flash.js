import request from '@/utils/request'
export function fetchList(params) {
  return request({
    url:'/flash/search',
    method:'get',
    params:params
  })
}

export function fetchListSearch(params) {
  return request({
    url:'/flash/search',
    method:'get',
    params:params
  })
}
export function updateStatus(params) {
  return request({
    url:'/flash/switchPro',
    method:'post',
    params:params
  })
}
export function deleteFlash(data) {
  return request({
    url:'/flash/delete',
    method:'post',
    data:data,
    
    // headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
  })
}
export function createFlash(data) {
  return request({
    url:'/flash/add',
    method:'post',
    data:data,
    // headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
  })
}
export function updateFlash(data) {
  return request({
    url:'/flash/update',
    method:'post',
    data:data,
    // headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
  })
}
