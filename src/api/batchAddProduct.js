import request from '@/utils/request'

export function upload(data) {
    return request({
      url:'/sproduct/upload',
      method:'post',
      data:data,
      headers: {
        'Content-Type': 'multipart/form-data'//发送的数据类型（默认是json）
       },
    })
  }
  

  // export function fetchList(data) {
  //   return request({
  //     url:'/sproduct/import',
  //     method:'get',
  //     data:data
  //   })
  // }

  export function productIsExist(data) {
    return request({
      url:'/product/isExist',
      method:'post',
      params:data,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
       },
    })
  }

  export function fetchList(data) {
    return request({
      url:'/sproduct/import',
      method:'post',
      data:JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
       },
    })
  }

  export function getAllData(data) {
    return request({
      url:'/sproduct/getAll',
      method:'post',
      data:data,
      // headers: {
      //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
      //  },
    })
  }