import request from '@/utils/request'
import { data } from 'autoprefixer'

export function fetchList(params) {
  return request({
    url: '/flash/listTime',
    method: 'get',
    params: params
  })
}

export function fetchSelectList(params) {
  return request({
    url: '/flash/selectTime',
    method: 'get',
    params: params
  })
}

export function updateStatus(params) {
  return request({
    url: '/flash/switchTime',
    method: 'post',
    params: params
  })
}

export function deleteSession(data) {
  return request({
    url: '/flash/deleteTime',
    method: 'post',
    data:data,
    // headers: {
    //   'Content-Type': 'application/x-www-form-urlencoded'//发送的数据类型（默认是json）
    //  },
  })
}

export function createSession(data) {
  return request({
    url: '/flash/addTime',
    method: 'post',
    data: data
  })
}

export function updateSession(data) {
  return request({
    url: 'flash/updateTime',
    method: 'post',
    data:data,
    // headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
  })
}
