import request from '@/utils/request'
export function getRaddarChart() {
  return request({
    url:'/orders/getOrderStatusData',
    method:'get',
  })
}

export function panelGroup() {
  return request({
    url:'/orders/getSaleData',
    method:'get',
  })
}

//批量获取sproduct表的商品和销量
export function getPieChart() {
    return request({
      url:'/sproduct/getSproductSale',
      method:'get',
      // headers: {
      //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
      //  },
    })
  }
  
  //批量获取sproduct表的商品名字
  export function getPieChartName() {
    return request({
      url:'/sproduct/getSproductName',
      method:'get',
      // headers: {
      //     'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
      //    },
    })
  }
  
  export function getLineChart(data) {
    return request({
      url:'/orders/getTurnoverData',
      method:'get',
      params:data,
    //   headers: {
    //    'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
    })
  }
  