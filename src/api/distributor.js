import request from '@/utils/request'

export function fetchList(params) {
  return request({
    url:'/riderm/list',
    method:'get',
    params:params
  })
}

export function addRider(data) {
  return request({
    url:'/riderm/add',
    method:'post',
    data:data
  })
}

export function updateRider(data) {
  return request({
    url:'/riderm/update',
    method:'post',
    data:data
  })
}
 
export function updatePwd(data) {
  return request({
    url:'/riderm/changePwd',
    method:'post',
    data:data
  })
}


export function changeStatus(data) {
  return request({
    url:'/riderm/cgstatus',
    method:'post',
    data:data
  })
}

export function deleteRider(data){
  return request({
    url:'/riderm/delete',
    method:'post',
    data:data
  })
}