import request from '@/utils/request'
export function getOrderSetting() {
  return request({
    url:'/orders/getSetting',
    method:'get',
  })
}

export function updateOrderSetting(params) {
  return request({
    url:'/orders/set',
    method:'post',
    params:params
  })
}
