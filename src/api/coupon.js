import request from '@/utils/request'
export function fetchList(data) {
  return request({
    url:'/coupon/search',
    method:'get',
    params:data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
     },
  })
}

export function createCoupon(data) {
  return request({
    url:'coupon/add',
    method:'post',
    data:JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
     },
  })
}

export function getCoupon(data) {
  return request({
    url:'/coupon/searchById',
    method:'get',
    params:data,
    // headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
  })
}

export function updateCoupon(data) {
  return request({
    url:'/coupon/update',
    method:'post',
    data:JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
     },
  })
}

export function deleteCoupon(data) {
  return request({
    url:'/coupon/delete',
    method:'post',
    data:data
  })
}
