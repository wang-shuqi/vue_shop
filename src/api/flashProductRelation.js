import request from '@/utils/request'
export function fetchList(data) {
  return request({
    url:'/flash/getProductByTime',
    method:'get',
    params:data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
     },
  })
}

export function fetchProductList(data) {
  return request({
    url:'/flash/searchByName',
    method:'get',
    params:data,
    // headers: {
    //   'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
    //  },
  })
}
// export function fetchSearchList(params) {
//   return request({
//     url:'/flash/searchByName',
//     method:'get',
//     params:params
//   })
// }
export function createFlashProductRelation(data) {
  return request({
    url:'/flash/addFlashProduct',
    method:'post',
    data:data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'//发送的数据类型（默认是json）
     },
  })
}
export function deleteFlashProductRelation(data) {
  return request({
    url:'/flash/deleteFlashProduct',
    method:'post',
    data:data
  })
}
export function updateFlashProductRelation(data) {
  return request({
    url:'/flash/updateFlashProduct',
    method:'post',
    data:data
  })
}
