import  {Message} from 'element-ui'
import {getToken} from '../utils/auth'
import url from 'postcss-url'
let websock=null
let messageCallback=null
let errorCallback=null
let wsUrl=''
let userId=localStorage.getItem('userId')


//接收ws后端返回的数据
function websocketonmessage(e){
  if(e!=null || e!=undefined){
    console.log(e)
     messageCallback(e.data)
  }
}
//发起websocket连接
function websocketSend(agentData){
  setTimeout(()=>{
    if(websock.readyState===websock.OPEN){
      if(agentData!=undefined){
        websock.send(JSON.stringify(agentData))
      } 
    }
    if(websock.readyState===websock.CLOSED){
      console.log('websock.readyState=3')
      Message.error('ws连接异常，请稍后重试')
      errorCallback()
    }
  },500)
}
//关闭ws连接
function websocketclose(e){
  if(e&&e.code!=1000){
    Message.error('ws连接异常，请稍后重试')
    errorCallback()
  }
}
//建立连接
function websocketOpen(){
  console.log("连接成功")

}
//初始化websocket
function initWebSocket(){
  if(typeof(WebSocket)==='undefined'){
    Message.error('您的浏览器不支持WebSocket,无法获取数据')
    return false
  }
  //wsUrl.headers['Authorization']=getToken()
  //const token='token='+getToken()
  //ws请求完整地址
  //const requestWsUrl=wsUrl+'?'+token
  //const token=getToken()
  //const requestWsUrl=wsUrl+'?'+token
  const requestWsUrl=wsUrl
  websock=new WebSocket(requestWsUrl)
  websock.onmessage=function(e){
    websocketonmessage(e)
  }
  websock.onopen=function(){
    websocketOpen()
  }
  websock.onerror=function(){
    Message.error('ws连接异常，请稍后重试')
    errorCallback()
  }
  websock.onclose=function(e){
    websocketclose(e)
  }
}

/*
  发起websocket请求函数
  @param {string} url ws连接地址
  @param {Object} agentData 传给后台的参数
  @param {function} successCallback 接收到ws数据，对数据进行处理的回调函数
  @param {function} errCallback ws连接错误的回调函数
 */
export function sendWebsocket(url,agentData, successCallback, errCallback){
  wsUrl=url
  initWebSocket()
  messageCallback=successCallback
  errorCallback=errCallback
  //websocketSend(message,agentData)
}
export function send(agentData){
  websocketSend(agentData)
}

export function closeWebsocket(){
  if(websock){
    websock.close()
    websock.onclose()
  }
}